<?php

/**
 * @defgroup plugins_generic_backend backend plugin
 */

/**
 * @file index.php
 *
 * @ingroup plugins_generic_backend
 * @brief Wrapper for customised backend.
 *
 */
require_once('notifyUserGroupOnDecisionPlugin.inc.php');

return new notifyUserGroupOnDecisionPlugin();
