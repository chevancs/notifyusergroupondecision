# Notify Users on decision for OJS

## About

Send mail to users in selected UserGroups (based on a decisionInform option of userGroup) when a decision - Accept or Reject - is made

## System requirements

This plugin has been tested on OJS version 3.1.2 and 3.2.1.


## Installation

To install:
 - unpack the plugin archive to OJS's `plugins/generic` directory;

**Note**:
Make sure the name of the directory for the plugin is 'notifyUserGroupOnDecision'

 - enable the plugin by ticking the checkbox for "Send a mail to users in selected UserGroups when a decision is made" in the set of generic plugins available ('Settings' > 'Website' > 'Plugins' > 'Generic Plugins').
 - enable option by ticking the checkbox "Members of this group receive a mail when a decision is made" available for each role you want
