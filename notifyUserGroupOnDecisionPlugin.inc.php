<?php

/**
 * @file BackendPlugin.inc.php
 *
 * @class BackendPlugin
 * @ingroup plugins_generic_backendPlugin
 *
 * @brief Plugin customizing OJS's backend for Comptes rendus de l'Académie des sciences.
 */

import('lib.pkp.classes.plugins.GenericPlugin');

class notifyUserGroupOnDecisionPlugin extends GenericPlugin {

	//
	// Implement methods from Plugin
	//
	/**
	 * @copydoc Plugin::register()
	 */
	function register($category, $path, $mainContextId = null) {
		if (parent::register($category, $path, $mainContextId)) {
			if ($this->getEnabled($mainContextId)) {
				// hook to add option decisionInform when editing a userGroup 	
				HookRegistry::register('usergroupform::readuservars', array($this, 'userGroup'));
				HookRegistry::register('usergroupform::validate', array($this, 'userGroup'));
				HookRegistry::register('usergroupform::display', array($this, 'userGroup'));

				HookRegistry::register('usergroupdao::getAdditionalFieldNames', array($this, 'userGroupSupplementaryFields'));
				
			
				HookRegistry::register('EditorAction::recordDecision', array($this, 'hookrecordDecision'));
				
			}
			return true;
		}
		return false;
	}

	/**
	 * @copydoc Plugin::getDescription()
	 */
	public function getDescription() {
		return __('plugins.generic.notifyUserGroupOnDecision.description');
	}

	/**
	 * @copydoc Plugin::getDisplayName()
	 */
	public function getDisplayName() {
		return __('plugins.generic.notifyUserGroupOnDecision.displayName');
	}
	
	// Override methods from LazyLoadPlugin
	//
	/**
	 * @copydoc LazyLoadPlugin::setEnabled()
	 */
	function setEnabled($enabled) {
		// force recompilation of templates
		$request = Application::getRequest();
		$templateMgr = TemplateManager::getManager($request);
		$templateMgr->clearTemplateCache();

		return parent::setEnabled($enabled);
	}

	/**
	 * Remove ask for notifications 
	 *
	 * @param $hookName string submissionsubmitstep4form::display
	 * @param $args array [
	 *  @option $form SubmissionSubmitStep4Form the form
	 * ]
	 */
	public function hookrecordDecision($hookName, $args) {
		
		//HookRegistry::call('EditorAction::recordDecision', array(&$submission, &$editorDecision, &$result, &$recommendation)
		$submission = $args[0]; 
		$decision = $args[1]["decision"]; 
		$recommendation = $args[3];
		if ( !$recommendation && ($decision == SUBMISSION_EDITOR_DECISION_ACCEPT || $decision==SUBMISSION_EDITOR_DECISION_DECLINE 
				|| $decision == SUBMISSION_EDITOR_DECISION_INITIAL_DECLINE ) ) {
			//envoi de mail aux groupes ayant l'option decisionInform

				import('classes.mail.ArticleMailTemplate');
				$email = new ArticleMailTemplate($submission, 'NOTIFICATION_CENTER_DEFAULT');

				$contextId = $submission->getContextId();
				$userGroupDao = DAORegistry::getDAO('UserGroupDAO');
				$userGroups = $userGroupDao->getByContextId($contextId) ;
				$decisionInformGroupIds = [];
				
				while( $userGroup=$userGroups->next() ) {
					$decisionInform = $userGroup->getData('decisionInform');
					if ( $decisionInform ) {
						$decisionInformGroupIds[] = $userGroup->getId();
					}
				}
				// on ajoute la liste des participants
				foreach ($decisionInformGroupIds as $groupId) {
					$group = $userGroupDao->getById($groupId, $contextId);
					$members = $userGroupDao->getUsersById($groupId, $contextId);
					while ($membre = $members->next()) {
						$email->addRecipient($membre->getEmail(), $membre->getFullName());
					}	
				}

				if ( $email->getRecipients() && sizeof($email->getRecipients() ) > 0 ) {

					if ( $decision == SUBMISSION_EDITOR_DECISION_ACCEPT ) $label = "acceptée";
					else $label = "refusée";
					$request = Application::getRequest();

					import('classes.core.ServicesContainer');
					$submissionUrl = ServicesContainer::instance()->get('submission')->getWorkflowUrlByUserRoles($submission);

					$email->setBody('La soumission '.$submission->getLocalizedFullTitle().' a été '.$label.'<br>Lien vers la soumission : '.$submissionUrl);
					$email->assignParams();
					if (!$email->send($request)) {
						import('classes.notification.NotificationManager');
						$notificationMgr = new NotificationManager();
						$notificationMgr->createTrivialNotification($request->getUser()->getId(), NOTIFICATION_TYPE_ERROR, array('contents' => __('email.compose.error')));
					}
				}

			}
		
	}
	
	/**
	 * Add option showInQueries to userGroup
	 */
	function userGroupSupplementaryFields($hookName, $params) {
		$fields =& $params[1];
		$fields[] = 'decisionInform';
		return false;
	}
	
	/**
	 * Add option decisionInform to userGroup
	 */
	function userGroup($hookname, $args) {
		switch ($hookname) {
			case 'usergroupform::readuservars':
				$vars =& $args[1];
				$vars[] = 'decisionInform';
				break;
			case 'usergroupform::display':
				$userGroupForm = $args[0];
				$request = Application::getRequest();
				$templateMgr = TemplateManager::getManager($request);
				$context = $request->getContext();
				$compile_dir = $templateMgr->getCompileDir().'/MERSENNE/'.$context->getPath();
				$templateMgr->setCompileDir($compile_dir);
				$templateMgr->registerFilter('pre', array($this, 'decisionInformPrefilter'));
				// recup l'id du usergroup si existe 
				if ( $userGroupForm->getUserGroupId() ) {
					$userGroupId = $userGroupForm->getUserGroupId();
					$userGroupDao = DAORegistry::getDAO('UserGroupDAO');
					$userGroup = $userGroupDao->getById($userGroupId);
					$decisionInform = $userGroup->getData('decisionInform'); 
					
					$templateMgr->assign('decisionInform', $decisionInform);
				} else {
					// query must be saved before updated in usergroupform::validate step
					$templateMgr->assign('newUG', true);
				}
				break;
			case 'usergroupform::validate':
				$userGroupForm = $args[0];
				if ($userGroupForm->isValid() && $userGroupForm->getUserGroupId() ) {
				// we save the query with new field here
				// because no hook is available in usergroupform::execute !!
					$userGroupId = $userGroupForm->getUserGroupId();
					$userGroupDao = DAORegistry::getDAO('UserGroupDAO');
					$decisionInform = $userGroupForm->getData('decisionInform');
					$userGroup = $userGroupDao->getById($userGroupId);
					$userGroup->setData('decisionInform', $decisionInform); 
					$userGroupDao->updateObject($userGroup);

					}
				break;
		}
	}
	
	/**
	 * Smarty prefilter to add showInQueries option and 
	 * new Query information because query must be saved before updated in usergroupform::validate step
	 * @param $source string the template data
	 * @param $templateManager TemplateManager
	 * @return string the expunged template
	 */
	function decisionInformPrefilter($source, $templateManager) {
		// Note: assume there is no nested fbvFormSection
		if (preg_match(';templates/controllers/grid/settings/roles/form/userGroupForm.tpl;', $source)) {
			$source = preg_replace(';(\{fbvFormSection title="settings.roles.roleOptions" list="true"\});sU', '$1'
					. '{if $newUG}<b>{translate key="plugins.generic.notifyUserGroupOnDecision.saveBefore"}</b><br>{else}'
					. '{fbvElement type="checkbox" name="decisionInform" id="decisionInform" checked=$decisionInform label="plugins.generic.notifyUserGroupOnDecision.settings.decisionInform"}{/if}', $source);
			// job done, dump filter
			$templateManager->unregisterFilter('pre', __FUNCTION__);
		}
		return $source;
	}

}
