<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import('tests.ContentBaseTestCase');

class notifyUserGroupOnDecisionTest extends ContentBaseTestCase {
	
	
	protected function getAffectedTables() {
		return PKP_TEST_ENTIRE_DB;
	}

	/**
	 * on active le plugin.
	 */
	function enablePlugin() {
		// active le plugin
		$this->login('admin','admin');
		// Section settings
		$this->waitForElementPresent($selector='link=Settings');
		$this->click($selector);
		$this->waitForElementPresent($selector='link=Website');
		$this->click($selector);
		$this->waitForElementPresent($selector='link=Plugins');
		$this->click($selector);
		// click on select-cell-manageusersforcommitteemembersplugin-enabled5b8ea0b74217b
		$this->waitForElementPresent($selector='css=[id^=select-cell-notifyusergroupondecisionplugin-enabled]');
		$this->click($selector);
		// on wait : The plugin "..." has been enabled.
		$this->waitForTextPresent('The plugin "Send a mail to users in selected UserGroups when a decision is made" has been enabled.');
		$this->logOut();
	}
	
	/**
	 * enable plugin / enable option for group JE / take a decision / check if a mail with correct recipient is send
	 */
	function testNotifyUserGroupOnDecision() {
		$this->enablePlugin();
		
		//create uniq filename	
		$tmpfname = tempnam("/tmp",'FOO');
		file_put_contents('/tmp/FileNameTest.txt', $tmpfname);
		
		import('plugins.generic.onlyForMailTest.enablePluginOFMT');
		enablePluginOFMT::enableMailTestPlugin($this);
		
		$title = "Hydrologic Connectivity in the Edwards Aquifer between San Marcos Springs and Barton Springs during 2009 Drought Conditions";

		parent::logIn('admin', 'admin');
		
		$this->waitForElementPresent($selector='link=Roles');
		$this->click($selector);
		$this->waitForElementPresent($selector="css=[id^=component-grid-settings-roles-usergroupgrid-row-3-editUserGroup-button-]");
		$this->click($selector);
		$this->waitForElementPresent($selector="css=[id=decisionInform]");
		$this->click($selector);
		
		$this->click("css=[id^=submitFormButton-]");
		$this->logOut();
		$this->findSubmissionAsEditor('dbarnes', null, $title);
		$this->recordEditorialDecision('Accept Submission');
		$this->logOut();
		// valider l'envoi du mail
		sleep(3);
		$contents = file($tmpfname);
		foreach ($contents as $line) {
			//each line is a PHPMailer object
			$line = base64_decode($line);
			$phpMailer = unserialize($line); /* @var $phpMailer \PHPMailer\PHPMailer\PHPMailer */
			$this->assertTrue(is_a($phpMailer, 'PHPMailer\PHPMailer\PHPMailer'));
			$body = $phpMailer->Body;
			$stringToFind = "La soumission $title a été acceptée<br>Lien vers la soumission";
			if ( strpos($body, $stringToFind) !== FALSE ) {
				$recipients = $phpMailer->getToAddresses();
			//				.array (
			//  0 => 
			//  array (
			//    0 => 'dbarnes@mailinator.com',
			//    1 => 'Daniel Barnes',
			//  ),
			//)

				$this->assertTrue(sizeof($recipients) == 1);
				$this->assertTrue($recipients[0][0] == 'dbarnes@mailinator.com');
				
			}

		}
		
	}

}



		
		